const { User } = require('../db/models')
const bcrypt =require('bcrypt')
const passport = require('../lib/passport');
const { decode } = require('jsonwebtoken');


const index = async(req, res) => {
    const token = dataUser.token
    await res.render('home/rps')
}
const signup = (req,res)=>{res.render('auth/register');}
const signin = (req,res)=>{res.render('auth/login');}
const register = (req,res,next)=>{
    // console.log(req.body);
    User.register(req.body)
    .then((user)=>{res.status(200).json(user)})
    .catch((err)=>next(err.message))
}
const login = (req, res) => {
    
    User.authenticate (req.body)
        .then((user) => {dataUser={
                id:user.id,
                username:user.username,
                token:user.generateToken()
            }
        // res.json(dataUser)
       res.redirect('/api/v1/user')
        
    }).catch(err=>{ next(err)})
}

const logout = (req,res)=>{req.logout();
    res.status(200);
}

module.exports = {index, signup, signin,register,login,logout}