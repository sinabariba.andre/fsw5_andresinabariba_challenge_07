const { Admin} = require('../db/models')
const { User } = require('../db/models')
const bcrypt =require('bcrypt')
const passport = require('../lib/passport');

const register = (req, res, next) => {
    // Kita panggil static method register yang sudah kita buat tadi
    // const{username, password}=req.body;
   // Kita panggil static method register yang sudah kita buat tadi
   console.log(req.body);
 Admin.register (req.body)
 .then(() => {
 res.redirect ('/admin/login' )
 })
 .catch(err => next(err))
}

const login= passport.authenticate('local', {
    successRedirect: '/admin/dashboard',
    failureRedirect: '/admin/login',
    failureFlash:true
})

const index = (req, res) => {
    const admin= req.user;
    User.findAll()
    .then((user)=>{
        res.render('admin/index',{user,admin}) 
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
   }
const logout = (req,res)=>{req.logout();
    res.status(200).redirect('/admin/login');
}

module.exports = {register,login,index,logout}