var express = require('express');
var router = express.Router();
var adminController = require('../controllers/admin.controller');
const {authAdmin} = require('../middleware/restrict')
const passport = require('../lib/passport')



/* GET admin listing. */
router.get('/login', (req, res) => res.render('admin/login'))
router.post('/login',  adminController.login);
router.get('/dashboard', authAdmin, adminController.index)
router.get('/register', (req,res) => res.render('admin/register'))
router.post('/register',  adminController.register);
// logout
router.post('/logout', authAdmin, adminController.logout)


module.exports = router