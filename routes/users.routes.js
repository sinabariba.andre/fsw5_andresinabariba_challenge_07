var express = require('express');
var router = express.Router();
var userController = require('../controllers/user.controller');
const passport = require('../lib/passport')
const {restrict} = require('../middleware/restrict')

/* GET users listing. */
router.get('/', userController.index);
router.get('/signup', userController.signup);
router.get('/signin', userController.signin);
router.post('/login',  userController.login);
router.post('/register', userController.register);

module.exports = router;
