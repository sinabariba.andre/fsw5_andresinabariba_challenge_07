const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
const { Strategy : JwtStrategy, ExtractJwt } = require('passport-jwt')
const { User , Admin } = require('../db/models')

/* Fungsi untuk authentication */
const authenticateAdmin =async(username, password, done)=> {
    try {
     // Memanggil method kita yang tadi
     const user = await Admin.authenticate({ username, password })
     return done(null, user)
    }
    catch(err) {
     /* Parameter ketiga akan dilempar ke dalam flash */
     return done(null, false, { message: err.message })
    }
}
passport.use(new LocalStrategy({ 
    usernameField: 'username', passwordField: 'password' }, authenticateAdmin))
passport.serializeUser((user, done) => done(null, user.id))
passport.deserializeUser(async (id, done) => done(null, await Admin.findByPk(id)))
        
    

const options = {
    // fromAuthHeaderAsBearerToken()
    // jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'Binar Secret'
}

const authenticate = async(payload,done )=>{
    try{
        const user = await User.findByPk(payload.id)
        return done(null,user)
    }catch(error){
        return done(null,false, {message:error.message})
    }
}
passport.use(new JwtStrategy(options, authenticate))


module.exports=passport