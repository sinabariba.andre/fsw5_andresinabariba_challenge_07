// middleware
const passport = require('../lib/passport')
const jwt = require('jsonwebtoken');

// user menggunakan passport jwt
const restrict = async (req,res,next)=>{
    return await passport.authenticate('jwt',{session:false,})
(req,res,next)
}

// authentikasi admin menggunakan passport local
const authAdmin =(req, res, next) => {
    if (req.isAuthenticated()) return next()
    res.redirect('/admin/login')
    }
module.exports= {
    restrict,authAdmin
}