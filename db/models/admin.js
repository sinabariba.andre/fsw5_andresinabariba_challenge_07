'use strict';
const {
  Model
} = require('sequelize');

const bcrypt =require('bcrypt')
module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static encrypt = (password)=>{return bcrypt.hashSync(password, 10)}
    
     static register = ({username, password})=>{
      const encryptedPassword = this.encrypt(password)
      return this.create({username, password:encryptedPassword })
    }
     checkPassword = (password) => bcrypt.compareSync(password, this.password)
 
     static authenticate = async ({username, password})=>{
       try{
         const admin = await this.findOne({where:{username}})
         if(!admin){
           return Promise.reject("user not found")
         }
         const isPasswordValid = admin.checkPassword(password)
         if(!isPasswordValid){
           return Promise.reject("wrong password")
         }
         return Promise.resolve(admin)
       }catch(error){
         return Promise.reject(error.message)
       }
     }
  };
  Admin.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Admin',
  });
  return Admin;
};