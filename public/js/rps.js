const rock_img = document.querySelector(".rock");
const paper_img = document.querySelector(".paper");
const scissor_img = document.querySelector(".scissor");
const versus_div = document.querySelector(".versus");
const result_div = document.querySelector(".versus");
const text_vs = document.querySelector(".text-versus");
const refresh_img = document.querySelector('.refresh');
var compChoice_div = document.getElementById('comp');


class Rps {

    start() {

        let win = () => {

            setTimeout(function () {
                versus_div.classList.replace("versus", "result");
                text_vs.classList.remove("vs");
                text_vs.innerHTML = "Player 1 Win";

                setTimeout(function () {
                    versus_div.classList.replace("result", "versus");
                    text_vs.classList.add("vs");
                    text_vs.innerHTML = "VS";
                }, 3500);
            }, 3000);

        }

        let lose = () => {
            setTimeout(function () {
                versus_div.classList.replace("versus", "result");
                text_vs.classList.remove("vs");
                text_vs.innerHTML = "Com <br>Win";

                setTimeout(function () {
                    versus_div.classList.replace("result", "versus");
                    text_vs.classList.add("vs");
                    text_vs.innerHTML = "VS";
                }, 3500);
            }, 3000);
        }

        let draw = () => {
            setTimeout(function () {
                versus_div.classList.replace("versus", "kotak-draw");
                text_vs.classList.remove("vs");
                text_vs.innerHTML = "DRAW";

                setTimeout(function () {
                    versus_div.classList.replace("kotak-draw", "versus");
                    text_vs.classList.add("vs");
                    text_vs.innerHTML = "VS";
                }, 3500);
            }, 3000);
        }



        function getComputerChoice() {

            const choices = ["rock", "paper", "scissor"];
            const randomNumber = Math.floor(Math.random() * 3);
            setTimeout(function () {
                compChoice_div.children[randomNumber + 1].classList.add('hover');

                setTimeout(function () {
                    compChoice_div.children[randomNumber + 1].classList.remove('hover');
                }, 5000);
            }, 1500);
            return choices[randomNumber];
        }



        function game(userChoice) {

            const computerChoice = getComputerChoice();
            switch (userChoice + computerChoice) {
                case "rockscissor":
                case "paperrock":
                case "scissorpaper":
                    win();
                    break;
                case "rockpaper":
                case "paperscissor":
                case "scissorrock":
                    lose();
                    break;
                case "rockrock":
                case "paperpaper":
                case "scissorscissor":
                    draw();
                    break;
            }
        }

        function main() {


            rock_img.addEventListener("click", function () {
                game("rock");
                setTimeout(() => {

                    rock_img.classList.add('hover');
                    setTimeout(() => {
                        rock_img.classList.remove('hover');
                    }, 6500)

                }, 0);
            });
            paper_img.addEventListener("click", function () {
                game("paper");
                setTimeout(() => {

                    paper_img.classList.add('hover');
                    setTimeout(() => {
                        paper_img.classList.remove('hover');
                    }, 6500)

                }, 0);

            });
            scissor_img.addEventListener("click", function () {
                game("scissor");
                setTimeout(() => {

                    scissor_img.classList.add('hover');
                    setTimeout(() => {
                        scissor_img.classList.remove('hover');
                    }, 6500)

                }, 0);

            });


        }
        main();
    }

    restart() {
        refresh_img.addEventListener("click", function () {
            location.reload();
        });
    }

}


let newGame = new Rps();
newGame.start();
newGame.restart();