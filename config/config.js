const fs = require('fs');

module.exports = {
  development: {
    username: 'postgres',
    password: 'root',
    database: 'challenge_07_development',
    host: '127.0.0.1',
    dialect: 'postgres'
  },
  test: {
    username: 'challenge_07_test',
    password: null,
    database: 'challenge_07_test',
    host: '127.0.0.1',
    dialect: 'postgres'
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        ca: fs.readFileSync(__dirname + '/postgres-ca-master.crt')
      }
    }
  }
};

